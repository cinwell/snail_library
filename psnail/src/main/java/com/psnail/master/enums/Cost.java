package com.psnail.master.enums;

/**
 * 用户钱包消费、收入方式
 * 
 */
public enum Cost {
	ChongZhi("充值", 1, "+"), TiXian("提现", 2, "-"), ZhiChiWangZhan("赞助网站", 3, "-"), KaiTongVIP("开通VIP", 4, "-"), XuFeiVIP(
			"续费VIP", 5, "-"), ZiXunWenTi("咨询问题", 6, "-"), YuanChengBuShu("远程部署", 7,
					"-"), ChaKanDaAn("查看答案", 8, "-"), DaAnTiCheng("答案提成", 9, "+"), TiCheng("提成", 10, "+");

	// 成员变量
	private String name;
	private int index;
	private String state;

	// 构造方法，注意：构造方法不能为public，因为enum并不可以被实例化
	private Cost(String name, int index, String state) {
		this.name = name;
		this.index = index;
		this.state = state;
	}

	// 普通方法
	public static String getCost(int index) {
		for (Cost c : Cost.values()) {
			if (c.getIndex() == index) {
				return c.name + c.state;
			}
		}
		return null;
	}

	// get set 方法
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public static void main(String[] args) {
		System.out.println(Cost.getCost(3));
	}
}
