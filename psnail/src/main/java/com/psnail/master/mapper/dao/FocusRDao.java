package com.psnail.master.mapper.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.psnail.master.entity.dto.FocusR;

public interface FocusRDao {

	List<FocusR> selectByWayTop(@Param("way") int way, @Param("top") int top);
}