package com.psnail.master.mapper.dao;

import com.psnail.master.entity.User;

public interface UserDao {

	User selectByEmail(String email);

	int countByAll();

	int countByAuthor();

}