package com.psnail.master.mapper;

import com.psnail.master.entity.FocusR;
import com.psnail.master.entity.FocusRExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FocusRMapper {
    int countByExample(FocusRExample example);

    int deleteByExample(FocusRExample example);

    int insert(FocusR record);

    int insertSelective(FocusR record);

    List<FocusR> selectByExample(FocusRExample example);

    int updateByExampleSelective(@Param("record") FocusR record, @Param("example") FocusRExample example);

    int updateByExample(@Param("record") FocusR record, @Param("example") FocusRExample example);
}