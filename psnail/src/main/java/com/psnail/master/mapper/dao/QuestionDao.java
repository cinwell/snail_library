package com.psnail.master.mapper.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.psnail.master.entity.Question;

public interface QuestionDao {

	List<Question> selectAllAnswer(@Param("resource_id") String resource_id);
}