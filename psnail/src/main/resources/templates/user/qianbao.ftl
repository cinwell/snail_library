<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 解答咨询问题。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>钱包记录  - 蜗牛库</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">
    <link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/datatables/jquery.dataTables.min.css" />  

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <#include "public/index/top.ftl">
  <#include "public/index/left.ftl">
  <#if user?exists>
  <section id="main-content">
  <section class="wrapper">
  <div class="row">
	<div class="panel panel-white">
       <div class="panel-body">
           <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
				  我的钱包记录，如有问题请联系站长 <a target="_blank" href="https://wpa.qq.com/msgrd?v=3&uin=1181014088&site=qq&menu=yes"><img border="0" src="${base}/assets/icon_png/qq.gif" alt="联系站长" title="联系站长"/></a>          
                    <div class="content">
					        <div class="tab-content">
					            <div class="tab-pane fade in active">
					                <div class="list-group mail-list">
					                	<div class="panel panel-white">
						                    <div class="panel-body">
						                        <table class="table table-bordered table-striped">
						                            <thead>
						                            	<th>序号</th>
						                                <th>用户操作</th>
						                                <th>钱包余额</th>
						                                <th>时间</th>
						                            </thead>
						                            <tbody id="mycost"></tbody>
								                </table>
								             </div>
							            </div>
							        </div>
							    </div>
							</div>
                    </div>
               </div>
           </div>
   		   
       </div>
    </div>
  </div>
  </section>
  </section>
  <#else>
  <section id="main-content">
  <section class="wrapper">
  <div class="row">
	<div class="panel panel-white">
       <div class="panel-body">
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	尚未登录！
               </div>
           </div>
        </div>
    </div>
  </div>
  </section>      
  </section>      
  </#if>
  <#include "public/index/modal.ftl">
  
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="${base}/assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>

  <!--common script for all pages-->
  <script src="${base}/assets/js/common-scripts.js"></script>
  
  <script src="${base}/curoAdmin/js/datatables/jquery.dataTables.min.js"></script>  
  <script src="${base}/curoAdmin/js/datatables-demo.js"></script> 
  
  <script type="text/javascript" src="${base}/star/lib/jquery.raty.min.js"></script>
  <script type="text/javascript"> 
  $.ajax({
       url:'${base}/user/mycost',
       type:'post',
       contentType:'application/json;charset=UTF-8',
	   dataType:'json',
       success:function(data){
       		if(data.status==0){
	       		alert(data.message);
       		}else if(data.status==1){
       			$("#mycost").html(data.message);
       		}
       },
       error:function(){
			alert("服务器错误！请联系站长");
       }
     })
  </script>
  </body>
</html>
