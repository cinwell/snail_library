<#assign base=request.contextPath />
<li id="header_inbox_bar" class="dropdown">
    <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
        <img src="${base}/assets/icon_png/xiaoxi_small3.png"/>
        <span class="badge bg-theme">0</span>
    </a>
    <ul class="dropdown-menu extended inbox">
        <div class="notify-arrow notify-arrow-green"></div>
        <li>
            <p class="green">你有 0 条新消息</p>
        </li>
        <li>
            <a href="index.html#">
                <span class="photo"><img alt="avatar" src="${base}/assets/img/ui-zac.jpg"></span>
                <span class="subject">
                <span class="from">Zac Snider</span>
                <span class="time">Just now</span>
                </span>
                <span class="message">
                    Hi mate, how is everything?
                </span>
            </a>
        </li>
        <li>
            <a href="index.html#">
                <span class="photo"><img alt="avatar" src="${base}/assets/img/ui-divya.jpg"></span>
                <span class="subject">
                <span class="from">Divya Manian</span>
                <span class="time">40 mins.</span>
                </span>
                <span class="message">
                 Hi, I need your help with this.
                </span>
            </a>
        </li>
        <li>
            <a href="index.html#">
                <span class="photo"><img alt="avatar" src="${base}/assets/img/ui-danro.jpg"></span>
                <span class="subject">
                <span class="from">Dan Rogers</span>
                <span class="time">2 hrs.</span>
                </span>
                <span class="message">
                    Love your new Dashboard.
                </span>
            </a>
        </li>
        <li>
            <a href="index.html#">
                <span class="photo"><img alt="avatar" src="${base}/assets/img/ui-sherman.jpg"></span>
                <span class="subject">
                <span class="from">Dj Sherman</span>
                <span class="time">4 hrs.</span>
                </span>
                <span class="message">
                    Please, answer asap.
                </span>
            </a>
        </li>
        <li>
            <a href="index.html#">查看所有消息</a>
        </li>
    </ul>
</li>
