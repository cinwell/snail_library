<#assign base=request.contextPath />
<aside>
  <div id="sidebar"  class="nav-collapse ">
      <ul class="sidebar-menu" id="nav-accordion">
      	  <#if user?exists>	
      	  <p class="centered"><img src="${base}/user/${user.photo}" class="img-circle" style="width:60px;"></p>
      	  <h5 class="centered">${user.name}</h5>
      	  <#else>
      	  <p class="centered"><img src="${base}/assets/img/logo_60.png" class="img-circle"></p>
      	  <h5 class="centered">尚未登录</h5>
      	  </#if>
          <li class="mt">
              <a href="${base}/index.html">
                  <img src="${base}/assets/icon_png/shouye_small.png" style="margin-right:5px;margin-bottom:5px;"/>
                  <span>首页</span>
              </a>
          </li>
          <li class="mt">
              <a href="${base}/resources.html" >
                  <img src="${base}/assets/icon_png/sousuo_small.png" style="margin-right:5px;margin-bottom:5px;"/>
                  <span>免费源码</span>
              </a>
          </li>
          <li class="mt">
              <a href="${base}/demand.html" >
                  <img src="${base}/assets/icon_png/xuqiu_small.png" style="margin-right:5px;margin-bottom:5px;"/>
                  <span>免费需求</span>
              </a>
          </li>
          <#if user?exists>
		  <hr/>
		  <li class="sub-menu">
			 <a data-toggle="modal" href="#personalModal"><img src="${base}/assets/icon_png/touxiang_small.png" style="margin-right:5px;margin-bottom:5px;"/>个人信息</a>
          </li>
		  <li class="sub-menu">
			 <a href="" id="clearCache"><img src="${base}/assets/icon_png/huancun.png" style="margin-right:5px;margin-bottom:5px;"/>清除缓存</a>
          </li>
		  <li class="sub-menu">
			 <a href="" id="exit"><img src="${base}/assets/icon_png/tuichu_small.png" style="margin-right:5px;margin-bottom:5px;"/>退出账号</a>
          </li>
          <#else>
          <hr/>
          <li class="sub-menu">
              <a data-toggle="modal" href="#loginModal">
                  <img src="${base}/assets/icon_png/denglu.png" style="margin-right:5px;margin-bottom:5px;"/>
                  <span>登录</span>
              </a>
          </li>
          <li class="sub-menu">
              <a href="${base}/zhuce.html" >
                  <img src="${base}/assets/icon_png/zhuce1.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>
                  <span>注册</span>
              </a>
          </li>
          </#if>
      </ul>
  </div>
</aside>
<script src="${base}/assets/js/jquery.js"></script>
<script>
$(document).ready(function(){
    $("#exit").bind("click",function(){
        $.ajax({
           url:'${base}/user/exit',
           type:'post',
           contentType:'application/json;charset=UTF-8',
		   dataType:'json',
           success:function(data){
           		window.location.reload();
           },
           error:function(){
				alert("服务器错误！请联系站长");
           }
         });
    });
    $("#clearCache").bind("click",function(){
        $.ajax({
           url:'${base}/user/clearCache',
           type:'post',
           contentType:'application/json;charset=UTF-8',
		   dataType:'json',
           success:function(data){
           		alert("缓存清除成功！");
           		window.location.reload();
           },
           error:function(){
				alert("服务器错误！请联系站长");
           }
         });
    });
});
</script>
