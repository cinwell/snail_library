<#assign base=request.contextPath />
<div class="col-sm-3 col-md-2 mg-btm-30">
<#if user?exists>
	<#if (user.author==1)>
    	<a class="btn btn-danger btn-sm btn-block" role="button" onclick="$('#fabuyuanma').show();$('#resourceContent').hide();">发布源码</a>
    <#else>
		<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=1181014088&site=qq&menu=yes" class="btn btn-danger btn-sm btn-block" role="button">申请源码作者</a>
    </#if>
<#else>
	<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=1181014088&site=qq&menu=yes"  class="btn btn-danger btn-sm btn-block" role="button" >申请源码作者</a>
</#if>
<hr style="margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;"/>
<h6>发现</h6>
<ul class="nav nav-pills nav-stacked">
    <li>
        <a href="${base}/resources.html">
			<img src="${base}/assets/icon_png/quanbu.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>最近源码</a>
        </a>
    </li>
    <li>
        <a href="${base}/resources-like.html">
            <img src="${base}/assets/icon_png/xihuan.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>喜欢最多
        </a>
    </li>
    <li>
        <a href="${base}/resources-down.html">
            <img src="${base}/assets/icon_png/xiazai.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>下载最多</a>
        </a>
    </li>
    <li>
        <a href="${base}/resources-show.html">
            <img src="${base}/assets/icon_png/chakan.png" style="width:15px;height:15px;margin-right:5px;margin-bottom:5px;"/>演示最多</a>
        </a>
    </li>
</ul>

<#include "soft.ftl">

</div>
<div class="col-sm-9 col-md-10">
	<div class="panel panel-white" id="fabuyuanma" style="display:none;">
       <div class="panel-body">
       	   <#if user?exists>
	       	   <#if (user.author==1)>
	       	   	   <div class="form-group">
				   	   <div class="alert alert-theme alert-success fade in">
		                    <div class="left">
		                    	<i class="fa fa-check icon"></i>
		                    </div> 
		                    <div class="content">
		                    	您是<strong>源码作者</strong>！请详细描述源码信息。
		                    </div>
		                    <div class="right">
		                       	了解：源码作者钱包满100元，可申请提现。
		                    </div>
		               </div>
		           </div>
		    	   <div class="form-group">
		               <label for="exampleInputEmail1">源码标题</label>
		               <input type="text" class="form-control" id="title" placeholder="">
		           </div>
		           <div class="form-group">
		               <label for="exampleInputPassword1">源码详细描述</label>
		           	   <script type="text/plain" id="myResource"></script>
		           </div>
		           <div class="form-group">
		               <label for="exampleInputEmail1">采用的技术，如：jsp、ssh、ssm！</label>
		               <input type="text" class="form-control" id="label" placeholder="">
		           </div>
		           <div class="form-group">
		               <label for="exampleInputEmail1">源码背景图(768*300)<br/>操作步骤：<br/>1、在上方的编辑器中上传图片。<br/>2、点击编辑器中HTML源代码按钮查看元素。<br/>3、复制图片地址，地址格式：https://xxx/user/xxx@qq.com/xxx.png。</label>
		               <input type="text" class="form-control" id="background" placeholder="">
		           </div>
		           <div class="form-group">
		               <label for="exampleInputEmail1">在线演示地址</label>
		               <input type="text" class="form-control" id="address" placeholder="">
		           </div>
		           <div class="form-group">
		               <label for="exampleInputEmail1">github下载</label>
		               <input type="text" class="form-control" id="github" placeholder="">
		           </div>
		           <div class="form-group">
		               <label for="exampleInputEmail1">git下载</label>
		               <input type="text" class="form-control" id="git" placeholder="">
		           </div>
		           <div class="form-group">
		               <label for="exampleInputEmail1">百度云下载</label>
		               <input type="text" class="form-control" id="baiduyun" placeholder="">
		           </div>
		           <button class="btn btn-green btn-sm" onclick="sendResource()"><i class="fa fa-location-arrow"></i> 发布</button>
		           <button class="btn btn-default btn-sm" onclick="$('#fabuyuanma').hide();$('#resourceContent').show();"> 取消</button>
		       <#else>
				  <div class="form-group">
	           	     <div class="alert alert-theme alert-info fade in">
	                    <div class="left">
	                        <i class="fa fa-info-circle icon"></i>
	                    </div> 
	                    <div class="content"> 
	                        <strong>不是源码作者！</strong>
	                        <button onclick="$('#fabuyuanma').hide();$('#resourceContent').show();"> 返回</button>
	                    </div>
	                 </div>
		          </div>
		       </#if> 
       	   <#else> 
       	   <div class="form-group">
           	   <div class="alert alert-theme alert-info fade in">
                    <div class="left">
                        <i class="fa fa-info-circle icon"></i>
                    </div> 
                    <div class="content"> 
                        <strong>尚未登录！</strong>
                        <button onclick="$('#fabuyuanma').hide();$('#resourceContent').show();"> 返回</button>
                    </div>
               </div>
           </div>
           </#if>
       </div>
    </div>
</div>
