<#assign base=request.contextPath />
<div class="col-sm-3 col-md-2 mg-btm-30">
    <a class="btn btn-danger btn-sm btn-block" onclick="$('#fabuxuqiu').show();$('#demandContent').hide();">发布需求</a>
    <hr style="margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;"/>
    <h6>需求</h6>
    <ul class="nav nav-pills nav-stacked">
    	<input type="hidden" id="mystate" name="mystate" value="0">
        <li>
            <a href="${base}/demand.html">
                <span class="badge bg-light pull-right">
					<#include "main/mainNumber.ftl">
				</span>所有
            </a>
        </li>
        <li>
            <a href="${base}/demand-wait.html">
                <span class="badge bg-light-blue pull-right">
					<#include "main/waitNumber.ftl">
				</span>待解决
            </a>
        </li>
        <li>
            <a href="${base}/demand-validate.html">
                <span class="badge bg-light-purple pull-right">
					<#include "main/validateNumber.ftl">
				</span>待验证
            </a>
        </li>
        <li>
            <a href="${base}/demand-complete.html">
                <span class="badge bg-light-green pull-right">
					<#include "main/completeNumber.ftl">
				</span>已解决
            </a>
        </li>
    </ul>
    <#--<hr style="margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eee;"/>
    <h6>优先</h6>
    <ul class="nav nav-pills nav-stacked">
        <li>
            <a href="${base}/demand-vip.html"><span class="badge bg-light-orange pull-right">
				<#include "main/vipNumber.ftl">
			</span>VIP 用户</a>
        </li>
        <li>
            <a href="${base}/demand-support.html"><span class="badge bg-light-green pull-right">
				<#include "main/supportNumber.ftl">
			</span>赞助用户</a>
        </li>
    </ul>-->
</div>

<div class="col-sm-9 col-md-10">
	<div class="panel panel-white" id="fabuxuqiu" style="display:none;">
       <div class="panel-body">
       	   <#if user?exists>
           <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                    <div class="left">
                        <i class="fa fa-check icon"></i>
                    </div> 
                    <div class="content">
                        请详细描述您的需求，必要时可贴一些图片。
                    	<#--您是
                    	<strong>
	                    <#if (user.vip==1)>VIP用户 
						<#elseif (user.zanzhu==1)>赞助用户 
						<#else> 普通用户
						</#if> 
						</strong>
                    	！享有的发布权限为：
                        <strong>
                        	<span style="color:#428bca;text-decoration:underline;" class="popovers" data-trigger="hover" data-placement="bottom" data-original-title="发布次数限制规则：" data-content="1：普通用户可以发布需求1次 / 每月。2：赞助用户可以发布需求2次 / 每月。3：VIP 用户可以发布需求3次 / 每月。" >
							<#if (user.vip==1)>3 
							<#elseif (user.zanzhu==1)>2
							<#else> 1
							</#if> 
							次 /每月</span>
                        </strong>-->
                    </div>
                    <div class="right">
                       	小贴士：可加入qq群寻求帮助。
                    </div>
               </div>
           </div>
           <div class="form-group">
               <label for="exampleInputEmail1">需求标题</label>
               <input type="text" class="form-control" id="title" placeholder="">
           </div>
           <div class="form-group">
               <label for="exampleInputPassword1">详细描述</label>
           	   <script type="text/plain" id="myDemand"></script>
           </div>
           <button class="btn btn-green btn-sm" onclick="sendDemand()"><i class="fa fa-location-arrow"></i> 发布</button>
           <button class="btn btn-default btn-sm" onclick="$('#fabuxuqiu').hide();$('#demandContent').show();"> 取消</button>
       	   <#else> 
       	   <div class="form-group">
           	   <div class="alert alert-theme alert-info fade in">
                    <div class="left">
                        <i class="fa fa-info-circle icon"></i>
                    </div> 
                    <div class="content"> 
                        <strong>尚未登录！</strong>
                        <button onclick="$('#fabuxuqiu').hide();$('#demandContent').show();"> 返回</button>
                    </div>
               </div>
           </div>
           </#if>
       </div>
    </div>
</div>
