<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 解答咨询问题。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>充值  - 蜗牛库</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <#include "public/index/top.ftl">
  <#include "public/index/left.ftl">
  <#if user?exists>
  <section id="main-content">
  <section class="wrapper">
  <div class="row">
	<div class="panel panel-white">
       <div class="panel-body">
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	充值须知：
                    <div class="content">
                    	1：充值完成后概不退换，请须知。
                    	<br/>2：充值为人工操作，不能及时到账，请理解。
                    	<br/>3：转账完成后，请点击第四步的邮件通知，请勿测试。
                    	<br/>4：充值过程中有任何问题可联系站长 <a target="_blank" href="https://wpa.qq.com/msgrd?v=3&uin=1181014088&site=qq&menu=yes"><img border="0" src="${base}/assets/icon_png/qq.gif" alt="联系站长" title="联系站长"/></a>
                    </div>
               </div>
           </div>
           
           <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                    <div class="content">
                	    步骤一：选择充值方式
                	    <input type="radio" name="payway" value="weipay" checked>微信支付
						<input type="radio" name="payway" value="alipay">支付宝支付
                    </div>
               </div>
           </div>
           
           <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                    <div class="content">
                	    步骤二：选择充值金额
                      <select id="money" name="money" style="margin-left:5px;">   
	                    <option value="0" selected></option>   
	                    <option value="20">￥20</option>   
				        <option value="30">￥30</option>   
				        <option value="50">￥50</option>   
				        <option value="100">￥100</option>   
				      </select>   
                    </div>
               </div>
           </div>
           
           <div class="form-group" id="pay">
		   	   <div class="alert alert-theme alert-success fade in">
                    <div class="content">
                	    步骤三：转账（<span style="color:red;font-weight:bold;">请备注您的注册邮箱</span>）
                      <div id="qrcode"></div>
                    </div>
               </div>
           </div>
           
           <div class="form-group" id="email">
		   	   <div class="alert alert-theme alert-success fade in">
                    <div class="content">
                	    步骤四：转账后，可选择发送通知邮件，请勿测试。<br/><br/>
                	  <button class="btn btn-success" id="sendEmailButton" onclick="sendEmail()">我已转账，发送通知邮件</button>
                    </div>
               </div>
           </div>
   		   
       </div>
    </div>
  </div>
  </section>
  </section>
  <#else>
  <section id="main-content">
  <section class="wrapper">
  <div class="row">
	<div class="panel panel-white">
       <div class="panel-body">
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	尚未登录！
               </div>
           </div>
        </div>
    </div>
  </div>
  </section>      
  </section>      
  </#if>
  <#include "public/index/modal.ftl">
  
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="${base}/assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>

  <!--common script for all pages-->
  <script src="${base}/assets/js/common-scripts.js"></script>
  
  <script type="text/javascript" src="${base}/star/lib/jquery.raty.min.js"></script>
  <script type="text/javascript">  
    $(function(){
      $("select").bind("change",function () {  
    	var money = this.value;
    	var payway = $("input[name='payway']:checked").val();
        if (this.value!= "0"){  
            $("#pay").show();
            $("#email").show();
            if(payway=="weipay"){
	            $("#qrcode").html("<br/><img src='${base}/assets/icon_png/"+payway+money+".png' width='250px' height='350px' />");
            }else{
	            $("#qrcode").html("<br/><img src='${base}/assets/icon_png/"+payway+money+".jpg'  width='250px' height='375px' />");
            }
        }
      });  
	  $(":radio").click(function(){
	     var payway = $(this).val();
	     var money = $("#money").val();
	     if(money!="0"){
		     if(payway=="weipay"){
	            $("#qrcode").html("<br/><img src='${base}/assets/icon_png/"+payway+money+".png' width='250px' height='350px' />");
	         }else{
	            $("#qrcode").html("<br/><img src='${base}/assets/icon_png/"+payway+money+".jpg'  width='250px' height='375px' />");
	         }
	     }
	  });
	});  
	function sendEmail() {
		if (confirm("确定已转账？")) {  
            var payway = $("input[name='payway']:checked").val();
            var money = $("#money").val();
			var allData = {
　　　　　　　　　　  payway:payway,
				money:money
　　　　　　　　};
	        $.ajax({
               url:'${base}/user/chongzhi',
               type:'post',
               contentType:'application/json;charset=UTF-8',
			   dataType:'json',
               data:JSON.stringify(allData),
               beforeSend: function () {
			        // 禁用按钮防止重复提交，发送前响应
			        $("#sendEmailButton").attr({ disabled: "disabled" });
                    $('#sendEmailButton').text("通知邮件正在发送。。。");
			    },
               success:function(data){
               		alert(data.message);
               		if(data.status==0){
               			$('#sendEmailButton').removeAttr("disabled");
               		}else if(data.status==1){
	                    $('#sendEmailButton').text("通知邮件发送成功");
               		}
               },
               error:function(){
					alert("服务器错误！请联系站长");
					$('#sendEmailButton').removeAttr("disabled");
					$('#sendEmailButton').text("我已转账，发送通知邮件");
               }
	         });
        }
	}
  </script>  
  </body>
</html>
