<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 解答咨询问题。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>VIP中心  - 蜗牛库</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">
	<link rel="stylesheet" type="text/css" href="${base}/curoAdmin/css/datatables/jquery.dataTables.min.css" />  

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <#include "public/index/top.ftl">
  <#include "public/index/left.ftl">
  <#if user?exists>
  <section id="main-content">
  <section class="wrapper">
  <div class="row">
	<div class="panel panel-white">
       <div class="panel-body">
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	vip须知：
                    <div class="content">
                    	1：每位VIP会员有一个独享的VIP编号。
                    	<br/>2：VIP会员凭VIP编号可加入会员专属群。
                    	<br/>3：VIP会员为永久制，网站运营过程中永不过期。
                    	<br/>4：VIP依据会员数量定价，VIP用户越多定价越高。
                    	<br/>5：若您存在疑问，请联系站长  <a target="_blank" href="https://wpa.qq.com/msgrd?v=3&uin=1181014088&site=qq&menu=yes"><img border="0" src="${base}/assets/icon_png/qq.gif" alt="联系站长" title="联系站长"/></a>
                    </div>
               </div>
           </div>
           
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	当前vip定价：
                    <div class="content" id="price">
                    	<br/>1：第<span style="color:red;font-weight:bold;"> 1-20 </span>位定价<span style="color:red;font-weight:bold"> ￥10 </span>，剩<span style="color:red;font-weight:bold"> 19 </span>位。
                    </div>
                    <br/>
                    <button type="button" id="vipButton" class="btn btn-round btn-success">开通VIP</button>
               </div>
           </div>
           
           <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                    <div class="content">
			   	   		VIP列表，每隔1分钟更新一次。
					        <div class="tab-content">
					            <div class="tab-pane fade in active" id="all">
					                <div class="list-group mail-list">
					                	<div class="panel panel-white">
						                    <div class="panel-body">
						                        <table id="table-1" class="table table-bordered table-striped">
						                            <thead>
						                                <th>序号</th>
						                                <th>用户</th>
						                                <th>VIP编号</th>
						                                <th>注册时间</th>
						                            </thead>
						                            <tbody>
                  			<#include "public/index/right/main/vip.ftl">
							                    	</tbody>
								                </table>
								             </div>
							            </div>
							        </div>
							    </div>
							</div>
                    </div>
               </div>
           </div>
   		   
       </div>
    </div>
  </div>
  </section>
  </section>
  <#else>
  <section id="main-content">
  <section class="wrapper">
  <div class="row">
	<div class="panel panel-white">
       <div class="panel-body">
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	尚未登录！
               </div>
           </div>
        </div>
    </div>
  </div>
  </section>      
  </section>      
  </#if>
  <#include "public/index/modal.ftl">
  
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="${base}/assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>

  <!--common script for all pages-->
  <script src="${base}/assets/js/common-scripts.js"></script>
  
  <script type="text/javascript" src="${base}/star/lib/jquery.raty.min.js"></script>
  <script src="${base}/curoAdmin/js/datatables/jquery.dataTables.min.js"></script>  
  <script src="${base}/curoAdmin/js/datatables-demo.js"></script> 
  <script>
  $(document).ready(function(){
	$.ajax({
       url:'${base}/user/getNumberVip',
       type:'post',
       contentType:'application/json;charset=UTF-8',
	   dataType:'json',
       success:function(data){
       		var number = data.number;
       		var page = parseInt(number/20)+1;
       		var content="";
       		for(var i=1;i<=page;i++){
	       		var one = i;
	       		var two = 20*(i-1)+1;
	       		var three = 20*i;
	       		var four = 10+(i-1)*5;
	       		var five=0
	       		if(number>i*20){
		       		five= 0;
	       		}else{
	       			five= 20-(number-((i-1)*20));
	       		}
	       		if(five==0){
	       			content+="<br/><span style='text-decoration:line-through'>"+one+"：第<span style='color:red;font-weight:bold;'> "+two+"-"+three+" </span>位定价<span style='color:red;font-weight:bold'> ￥"+four+" </span>，剩<span style='color:red;font-weight:bold'> "+five+" </span>位。</span>";
	       		}else{
	       			content+="<br/>"+one+"：第<span style='color:red;font-weight:bold;'> "+two+"-"+three+" </span>位定价<span style='color:red;font-weight:bold'> ￥"+four+" </span>，剩<span style='color:red;font-weight:bold'> "+five+" </span>位。";
	       		}
       		}
       		
       		$("#price").html(content);
       },
       error:function(){
			alert("服务器错误！请联系站长");
       }
     })
  });
  $("#vipButton").bind("click",function(){
		if(confirm("确认开通VIP？")){
	        $.ajax({
	           url:'${base}/user/vip',
	           type:'post',
	           contentType:'application/json;charset=UTF-8',
			   dataType:'json',
	           beforeSend: function () {
			        // 禁用按钮防止重复提交，发送前响应
			        $("#vipButton").attr({ disabled: "disabled" });
	                $('#vipButton').text("正在处理。。。");
			    },
	           success:function(data){
	           		alert(data.message);
	           		if(data.status==0){
	           			$('#vipButton').removeAttr("disabled");
	           			$('#vipButton').text("开通VIP");
	           		}else if(data.status==1){
	                    window.location.reload();
	           		}
	           },
	           error:function(){
					alert("服务器错误！请联系站长");
					$('#vipButton').removeAttr("disabled");
					$('#vipButton').text("开通VIP");
	           }
	         })
	    }
	});
  </script>
  </body>
</html>
