<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 找到您需要的源码项目，在线演示和免费下载。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>设置新密码  - 蜗牛库</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <#include "public/index/top.ftl">
  <div id="login-page">
  	<div class="container">
      <div class="form-login">
        <h2 class="form-login-heading">设置新密码</h2>
        <div class="login-wrap">
            <input type="hidden" class="form-control" name="id" id="id" value="${RequestParameters["id"]}" placeholder="邮箱" disabled>
            <br>
            <input type="password" name="password" id="password" class="form-control" placeholder="密码">
            <br>
            <input type="password" name="password_confirm" id="password_confirm" class="form-control" placeholder="确认密码">
            <br>
            <button id="createButton" class="btn btn-theme btn-block" type="submit">确定</button>

        </div>
      </div>
  	</div>
  </div>
	  
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="${base}/assets/js/jquery.js"></script>
    <script src="${base}/assets/js/bootstrap.min.js"></script>
    <script src="${base}/assets/js/autoMail.1.0.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="${base}/assets/js/jquery.backstretch.min.js"></script>
    <script>
        //$.backstretch("背景图片路径", {speed: 500});
        $(document).ready(function(){
			$("#createButton").bind("click",function(){
                var id = $('#id').val();
                var password = $('#password').val();
                var password_confirm = $('#password_confirm').val();
		        if(id==""){
				    alert("验证id为空！");
				    $('#id').focus();
				    return false;
				}
				if(password==""){
					alert("密码不能为空！");
				    $('#password').focus();
				    return false;
				}else if(password.length<6||password.length>20){
					alert("密码应该为6-20位之间");
				    $('#password').focus();
				    return false;
				}
				if(password_confirm==""){
					alert("确认密码不能为空！");
				    $('#password_confirm').focus();
				    return false;
				}else if(password!=password_confirm){
					alert("两次密码输入不一致！");
				    $('#password_confirm').focus();
				    return false;
				}
				$('#createButton').attr("disabled", "disabled");
				var allData = {
	　　　　　　　　　　  	id:id,
               		password:password,
               		password_confirm:password_confirm
	　　　　　　　　};
		        $.ajax({
	               url:'${base}/user/newpassword',
	               type:'post',
	               contentType:'application/json;charset=UTF-8',
				   dataType:'json',
	               data:JSON.stringify(allData),
	               beforeSend: function () {
				        // 禁用按钮防止重复提交，发送前响应
				        $("#createButton").attr({ disabled: "disabled" });
	                    $('#createButton').text("正在设置新密码。。。");
				    },
	               success:function(data){
	               		alert(data.message);
	               		if(data.status==0){
	               			$('#createButton').removeAttr("disabled");
		                    $('#createButton').text(data.message);
	               		}else if(data.status==1){
		                    window.location.href = "${base}/login.html";
	               		}
	               },
	               error:function(){
						alert("服务器错误！请联系站长");
						$('#createButton').removeAttr("disabled");
	               }
		         });
            });
		});
    </script>

  </body>
</html>
