<#assign base=request.contextPath />
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="蜗牛库 - 解答咨询问题。">
    <meta name="author" content="蜗牛库">
    <meta name="keyword" content="源码, 免费下载, 在线演示, 技术支持, 咨询服务">

    <title>审查充值  - 蜗牛库</title>
    <link href="${base}/assets/img/logo.ico" rel="shortcut icon">

    <!-- Bootstrap core CSS -->
    <link href="${base}/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="${base}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="${base}/assets/css/style.css" rel="stylesheet">
    <link href="${base}/assets/css/style-responsive.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <#include "public/index/top.ftl">
  <#include "public/index/left.ftl">
  <#if user?exists>
  <section id="main-content">
  <section class="wrapper">
  <div class="row">
	<div class="panel panel-white">
       <div class="panel-body">
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	审查须知：
                    <div class="content">
                    	1：只有站长和管理员具有审查权限。
                    	<br/>2：管理员先和站长联系，确认用户是否转账、邮箱和充值金额等信息是否正确后再操作。<a target="_blank" href="https://wpa.qq.com/msgrd?v=3&uin=1181014088&site=qq&menu=yes"><img border="0" src="${base}/assets/icon_png/qq.gif" alt="联系站长" title="联系站长"/></a>
                    </div>
               </div>
           </div>
           <input type="hidden" id="id" value="${RequestParameters["id"]}"/>
	           
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	请确认以下信息
                    <div class="content">
                    	<br/>用户昵称：<span id="name"></span>
                    	<br/>用户邮箱：<span id="email"></span>
                    	<br/>转账方式：<span id="payway"></span>
                    	<br/>转账金额：￥<span id="money"></span>元
                    	<br/>转账时间：<span id="time"></span>
                    </div>
                	<br/>
                   	<button type="button" id="yes1" onclick="audit(1)" class="btn btn-round btn-success">充值</button>
		   	   		<button type="button" id="yes2" onclick="audit(2)" class="btn btn-round btn-danger">不充值</button>
               </div>
           </div>
           
   		   
       </div>
    </div>
  </div>
  </section>
  </section>
  <#else>
  <section id="main-content">
  <section class="wrapper">
  <div class="row">
	<div class="panel panel-white">
       <div class="panel-body">
   	   	   <div class="form-group">
		   	   <div class="alert alert-theme alert-success fade in">
                	尚未登录！
               </div>
           </div>
        </div>
    </div>
  </div>
  </section>      
  </section>      
  </#if>
  <#include "public/index/modal.ftl">
  
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="${base}/assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="${base}/assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="${base}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="${base}/assets/js/jquery.nicescroll.js" type="text/javascript"></script>

  <!--common script for all pages-->
  <script src="${base}/assets/js/common-scripts.js"></script>
  
  <script type="text/javascript" src="${base}/star/lib/jquery.raty.min.js"></script>
  <script type="text/javascript"> 
  $(document).ready(function(){
  		var id =  $("#id").val();
		var allData = {
　　　　　　　　id:id
　　　　　};
  		$.ajax({
               url:'${base}/user/getPayrecord',
               type:'post',
               contentType:'application/json;charset=UTF-8',
			   dataType:'json',
               data:JSON.stringify(allData),
               success:function(data){
               		if(data.status==1){
	                    $("#name").html(data.message.name);
	                    $("#email").html(data.message.email);
	                    $("#payway").html(data.message.payway);
	                    $("#money").html(data.message.money);
	                    $("#time").html(data.time);
               		}else{
	       				alert(data.message);
               		}
               },
               error:function(){
					alert("服务器错误！请联系站长");
               }
	         }); 
    }); 
    
	function audit(state) {
		var msg;
		if(state==1){
			msg="充值";
		}else{
			msg="不充值"
		}
		if(confirm("确认"+msg+"？")){
			var id = $("#id").val();
	        var allData = {
　　　　　　　　　　 id:id,
				state:state
　　　　　　　　};
	        $.ajax({
               url:'${base}/user/adminAudit',
               type:'post',
               contentType:'application/json;charset=UTF-8',
			   dataType:'json',
               data:JSON.stringify(allData),
               beforeSend: function () {
			        // 禁用按钮防止重复提交，发送前响应
			        $("#yes1").attr({ disabled: "disabled" });
			        $("#yes2").attr({ disabled: "disabled" });
                    $('#yes'+state).text("正在处理。。。");
			    },
               success:function(data){
               		alert(data.message);
               		if(data.status==0){
               			$('#yes1').removeAttr("disabled");
               			$('#yes2').removeAttr("disabled");
	                    $('#yes'+state).text(msg);
               		}else if(data.status==1){
                    	$('#yes'+state).text("操作完成。。。");
               		}
               },
               error:function(){
					alert("服务器错误！请联系站长");
					$('#yes1').removeAttr("disabled");
					$('#yes2').removeAttr("disabled");
                    $('#yes'+state).text(msg);
               }
	         });
		}
	}
  </script>
  </body>
</html>
