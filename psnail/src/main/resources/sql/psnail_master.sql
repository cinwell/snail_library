/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2018/3/25 21:43:54                           */
/*==============================================================*/


drop table if exists answer;

drop table if exists cost;

drop table if exists demand;

drop table if exists focus_d;

drop table if exists focus_r;

drop table if exists payrecord;

drop table if exists question;

drop table if exists resource;

drop table if exists user;

/*==============================================================*/
/* Table: answer                                                */
/*==============================================================*/
create table answer
(
   id                   varchar(32) not null comment 'id，唯一标识',
   content              varchar(10000) not null comment '解决方案内容',
   state                int not null comment '1：待验证；2：已验证，未解决；3：已验证，已解决',
   demand_id            varchar(32) not null comment '所归属的需求id',
   user_id              varchar(32) not null comment '解决方案提出的用户',
   time                 timestamp not null comment '解决方案时间',
   primary key (id)
);

alter table answer comment '需求解决方案表';

/*==============================================================*/
/* Table: cost                                                  */
/*==============================================================*/
create table cost
(
   id                   varchar(32) not null comment '外键，用户唯一标识id',
   way                  int not null comment '用户消费、收入的方式，对应在Cost枚举类',
   amount               numeric(12,2) not null comment '用户消费、收入的金额',
   balance              numeric(12,2) not null comment '用户钱包中的余额',
   time                 timestamp not null comment '用户消费、收入的时间'
);

alter table cost comment '花费记录表，用于记录用户充值、提现、赞助、消费等信息';

/*==============================================================*/
/* Table: demand                                                */
/*==============================================================*/
create table demand
(
   id                   varchar(32) not null comment '主键，唯一标识',
   title                varchar(100) not null comment '需求标题',
   content              varchar(10000) not null comment '需求详细内容描述',
   state                int not null comment '1：待解决；2：待验证；3：已解决',
   user_id              varchar(32) not null comment '提需求的用户',
   time                 timestamp not null comment '提需求时间',
   primary key (id)
);

alter table demand comment '需求表';

/*==============================================================*/
/* Table: focus_d                                               */
/*==============================================================*/
create table focus_d
(
   user_id              varchar(32) not null comment '用户id',
   user_email           varchar(50) not null comment '用户邮箱',
   demand_id            varchar(32) not null comment '需求id',
   time                 timestamp not null comment '用户关注需求时间'
);

alter table focus_d comment '需求关注表';

/*==============================================================*/
/* Table: focus_r                                               */
/*==============================================================*/
create table focus_r
(
   user_id              varchar(32) not null comment '用户id',
   resource_id          varchar(32) not null comment '源码id',
   way                  int not null comment '1：喜欢，2：下载，3：演示，4：远程部署',
   time                 timestamp not null comment '时间'
);

alter table focus_r comment '源码关注表
';

/*==============================================================*/
/* Table: payrecord                                             */
/*==============================================================*/
create table payrecord
(
   id                   varchar(32) not null comment '主键',
   name                 varchar(50) not null comment '用户昵称',
   email                varchar(50) not null comment '用户邮箱',
   payway               varchar(10) not null comment '支付方式',
   money                numeric(12,2) not null comment '转账金额',
   state                int not null comment '处理状态：0未处理、1充值、2不充值',
   admin                varchar(50) not null comment '管理员处理邮箱',
   time                 timestamp not null comment '时间',
   primary key (id)
);

alter table payrecord comment '支付通知邮件记录';

/*==============================================================*/
/* Table: question                                              */
/*==============================================================*/
create table question
(
   id                   varchar(32) not null comment '主键，唯一标识',
   title                varchar(1000) not null comment '问题标题',
   content              varchar(20000) not null comment '问题答案',
   chars                int not null comment '答案有多少个字符',
   star                 int not null comment '该问题答案评价几颗星',
   way                  int not null comment '1：技术咨询；2：初级部署；3：中级部署；4：高级部署',
   resource_id          varchar(32) not null comment '问题所属源码',
   user_id              varchar(32) not null comment '提问者',
   time                 timestamp not null comment '提问时间',
   primary key (id)
);

alter table question comment '问题表';

/*==============================================================*/
/* Table: resource                                              */
/*==============================================================*/
create table resource
(
   id                   varchar(32) not null comment '主键，唯一标识',
   title                varchar(100) not null comment '源码标题',
   content              varchar(10000) not null comment '源码描述',
   label              varchar(100) not null comment '源码标签',
   background           varchar(100) not null comment '源码背景图',
   address              varchar(100) not null comment '演示地址',
   github               varchar(100) not null comment 'github下载地址',
   git                  varchar(100) not null comment 'git下载地址',
   baiduyun             varchar(255) not null comment '百度云盘下载地址',
   official             varchar(255) not null comment '官方下载地址',
   user_id              varchar(32) not null comment '源码作者id',
   time                 timestamp not null comment '上传时间',
   primary key (id)
);

alter table resource comment '源码表';

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
   id                   varchar(32) not null comment '主键，用户唯一标识',
   email                varchar(50) not null comment '用户邮箱',
   name                 varchar(15) not null comment '用户昵称',
   password             varchar(32) not null comment '用户密码',
   photo                varchar(150) not null comment '用户头像',
   money                numeric(12,2) not null comment '用户钱包余额',
   author               int not null comment '0：不是源码作者，1：是源码作者',
   vip                  int not null comment '0：不是vip，1：是vip',
   zanzhu               int not null comment '0：无赞助，1：赞助者',
   state                int not null comment '0：账号不正常，1：账号正常',
   time                 timestamp not null comment '注册时间',
   primary key (id)
);

alter table user comment '用户表，用于网站注册、登录';

alter table answer add constraint FK_Reference_10 foreign key (user_id)
      references user (id) on delete restrict on update restrict;

alter table answer add constraint FK_Reference_9 foreign key (demand_id)
      references demand (id) on delete restrict on update restrict;

alter table cost add constraint FK_Reference_1 foreign key (id)
      references user (id) on delete restrict on update restrict;

alter table demand add constraint FK_Reference_6 foreign key (user_id)
      references user (id) on delete restrict on update restrict;

alter table focus_d add constraint FK_Reference_7 foreign key (user_id)
      references user (id) on delete restrict on update restrict;

alter table focus_d add constraint FK_Reference_8 foreign key (demand_id)
      references demand (id) on delete restrict on update restrict;

alter table focus_r add constraint FK_Reference_11 foreign key (user_id)
      references user (id) on delete restrict on update restrict;

alter table focus_r add constraint FK_Reference_12 foreign key (resource_id)
      references resource (id) on delete restrict on update restrict;

alter table question add constraint FK_Reference_4 foreign key (resource_id)
      references resource (id) on delete restrict on update restrict;

alter table question add constraint FK_Reference_5 foreign key (user_id)
      references user (id) on delete restrict on update restrict;

alter table resource add constraint FK_Reference_3 foreign key (user_id)
      references user (id) on delete restrict on update restrict;

